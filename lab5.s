	AREA interrupts, CODE, READWRITE
	EXPORT lab5
	EXPORT FIQ_Handler
	EXTERN uart_init
	EXTERN read_character
	EXTERN display_digit_on_7_seg
	EXTERN pin_connect_block_setup_for_uart0
	EXTERN output_character
	EXTERN output_string
	

prompt = "\n\r\n\r\t\tWelcome to lab #5\n\r",0
instructions = "This program displays decimal numbers 0 - 9 on the 7 segment display\n\rand allows the user to increment, decrement, or clear the display.\n\rInitially the display is turned off. To turn on the display and\n\renable keyboard input, press the push-button (P0.14 on the ARM board).\n\rWhen first turned on, the 7-seg display will show the value 0.\n\rTo turn off the display and disable keyboard input, depress the push-button again.\n\rThe currently displayed value on the 7-seg will be remembered\n\rand displayed on the 7-seg the next time the push button is pressed.\n\rWhen keyboard input is enabled, pressing the following keys does the following:\n\r\n\r0     Clear the display so that a zero (0) is displayed\n\r+     Increments the display by one. When 9 is reached, the display wraps around to 0.\n\r-     Decrements the display by one.  When 0 is reached, the display wraps around to 9.\n\r\n\rPress Q (capital Q) at ANY time to End the Program\n\r(even if other keyboard input had been disabled by pressing the push button).\n\rAll valid user input will be displayed to the screen.\n\r\n\r",0
quit_prompt = "\n\rYou have exited the program."
SEGSTATE = "\0",0      ; Current value being displayed to 7-seg, or value last displayed before being turned off. Initialized to 0.
UARTSTATE = "\0",0     ; 1 means we are accepting UART input, 0 means we are ignoring it. Initialized to 0.
Q_PRESSED = "\0",0     ; 1 means user has pressed 'Q' to quit program, 0 means they have not. Initialized to 0.


	ALIGN

lab5	 		STMFD sp!, {lr}

				; Call our setup functions
				BL pin_connect_block_setup_for_uart0
				BL uart_init
				BL interrupt_init

				; Initialize SEGSTATE to 0
				LDR r0, =SEGSTATE
				MOV r1, #0
				STRB r1, [r0]
				
				; Initialize UARTSTATE to 0
				LDR r0, =UARTSTATE
				MOV r1, #0
				STRB r1, [r0]

				; Initialize Q_PRESSED to 0
				LDR r0, =Q_PRESSED
				MOV r1, #0
				STRB r1, [r0]
				
				; 17 is the offset in the lookup table for display_digit_on_7_seg to clear the display
				MOV r0, #17
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL display_digit_on_7_seg
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				
				; Display prompt / welcome message
				LDR r4, =prompt
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL output_string
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				
				; Display user instructions
				LDR r4, =instructions
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL output_string
				LDMFD SP!, {r0-r12, lr}   ; Restore registers

				; We stay in this loop until the user presses 'Q' to QUIT to program				
loop			LDR r0, =Q_PRESSED	; Load Q_PRESSED address
				LDRB r1, [r0]		; Load Q_PRESSED memory contents (single byte)
				CMP r1, #1          ; Compare contents to 1
				BEQ	quit            ; If contents == 1, goto quit
				B loop              ; Else loop again and check if 'Q' was pressed
				
quit			BL disable_interrupts	; If user has QUIT the program, we need to disable all further interrupts
				
				; Display exit message
				LDR r4, =quit_prompt
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL output_string
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				
				; Return to caller
				LDMFD sp!,{lr}
				BX lr

interrupt_init       
				STMFD SP!, {r0-r1, lr}   ; Save registers 
				
				; Push button setup		 
				LDR r0, =0xE002C000
				LDR r1, [r0]
				ORR r1, r1, #0x20000000
				BIC r1, r1, #0x10000000
				STR r1, [r0]  ; PINSEL0 bits 29:28 = 10

				; Classify sources as IRQ or FIQ
				; 1 == FIQ, 0 == IRQ
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0xC]
				ORR r1, r1, #0x8000 ; External Interrupt 1
				ORR r1, r1, #0x40	; UART 0 interrupt
				STR r1, [r0, #0xC]

				; Enable Interrupts
				; 1 == enable, 0 == NO EFFECT
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0x10] 
				ORR r1, r1, #0x8000 ; External Interrupt 1
				ORR r1, r1, #0x40	; UART 0 Interrupt enable 
				STR r1, [r0, #0x10]

				;UART0 Interrupt When Data Recieved U0IER
				LDR r0, =0xE000C004		; Load U0EIR address
				LDR r1, [r0]			; Load U0EIR contents
				ORR r1, r1, #1			; Set bit 0 to 1 to enable Data recieved interrupt
				STR r1, [r0]			; Store contents back

				; External Interrupt 1 setup for edge sensitive
				LDR r0, =0xE01FC148
				LDR r1, [r0]
				ORR r1, r1, #2  ; EINT1 = Edge Sensitive
				STR r1, [r0]

				; Enable FIQ's, Disable IRQ's
				; 1 == disable, 0 == enable
				; Bit 6 is for FIQ, Bit 7 is for IRQ
				MRS r0, CPSR
				BIC r0, r0, #0x40     ; Clear bit 6 to enable FIQs
				ORR r0, r0, #0x80     ; Set bit 7 to disable IRQs
				MSR CPSR_c, r0

				LDMFD SP!, {r0-r1, lr} ; Restore registers
				BX lr             	   ; Return
				
disable_interrupts
				
				STMFD SP!, {r0-r12, lr}
				
				; Disable Interrupts
				; 1 == disable interrupt
				; 0xFFFFF014 is the interrupt diable register address
				LDR r0, =0xFFFFF000
				LDR r1, [r0, #0x14] 
				ORR r1, r1, #0x8000 ; Set bit to disable External Interrupt 1
				ORR r1, r1, #0x40	; UART 0 Interrupt disable
				STR r1, [r0, #0x14]
				
				; Disable FIQ's, Disable IRQ's
				; 1 == disable, 0 == enable
				MRS r0, CPSR
				ORR r0, r0, #0x40     ; Set bit 6 to disable FIQs
				ORR r0, r0, #0x80     ; Set bit 7 to disable IRQs
				MSR CPSR_c, r0
				
				LDMFD SP!, {r0-r12, lr}
				BX lr             	   ; Return
				

FIQ_Handler		STMFD SP!, {r0-r12, lr}   ; Save registers

EINT1			; Check for EINT1 interrupt
				; External Interrupt Flag Register (EXTINT) address
				; EINT0 - EINT3, Pins 0-3
				; 1 == Interrupt Pending
				LDR r0, =0xE01FC140    
				LDR r1, [r0]
				TST r1, #2      ; Note that TST takes the logical AND of r1 and #2, and sets CPSR upper four bits based on result
				BEQ UART0       ; If we have no EINT1 interrupt, goto UART0 to see if the interrupt came from UART0
			
				STMFD SP!, {r0-r12, lr}   ; Save registers 
					
				; Push button EINT1 Handling Code
				
				;Load 7-seg state
				LDR r0, =SEGSTATE
				LDRB r7, [r0]
				
				;Check flag to see if UART0 is enabled (if we are accepting UART0 input)
				LDR r0, =UARTSTATE
				LDRB r1, [r0]
				CMP r1, #1
				BEQ seven_seg_off    ; We are servicing a push-button interrupt, so if the UART0 state is currently enabled, we need to disable both it and the 7-seg

				; Set UART to ON (because the UART state was previously checked to be off -- push button toggles on/off)
				MOV r1, #1
				STRB r1, [r0]        ; here r0 is still UARTSTATE flag address
				
				; Push button toggled UART and 7-seg to turn back ON, 
				; so need to display previous value on 7-seg to 7-seg.
				MOV r0, r7
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL display_digit_on_7_seg
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				
				B continue           ; We turned on the UART and 7-seg, so skip over seven_seg_off code
				
		
seven_seg_off	; Set UART state flag to OFF
				LDR r0, =UARTSTATE
				MOV r1, #0
				STRB r1, [r0]
				
				; Turn 7-seg off
				; 17 is the offset in the lookup table for display_digit_on_7_seg to clear the display
				MOV r0, #17
				STMFD SP!, {r0-r12, lr}   ; Save registers
				BL display_digit_on_7_seg
				LDMFD SP!, {r0-r12, lr}   ; Restore registers

				; Used to clear the EINT1 (push button) interrupt
				; 
continue		LDMFD SP!, {r0-r12, lr}   ; Restore registers
				
				ORR r1, r1, #2		; Clear Interrupt
				STR r1, [r0]		; r0 == External Interrupt Flag Register
									; Write a 1 to clear interrupt
				
				B FIQ_Exit
				
UART0			STMFD SP!, {r0-r12, lr}   ; Save registers

				; CHeck for UART0 interrupt
				; 0 in bit 0 of U0IIR == Interrupt Pending, 1 == no interrupt
				LDR r0, =0xE000C008			; r0 == UART Interrupt Identification Register (U0IIR)
				LDR r1, [r0]
				TST r1, #1					; If 1 in bit 0, z == 0 there is no interrupt in UART0
				BNE FIQ_Exit				
				
				; Check if ignoring UART0 input
				; Q still works
				LDR r0, =UARTSTATE
				LDRB r1, [r0]
				CMP r1, #1
				BNE only_Q
				
				STMFD SP!, {r1-r12, lr}   ; Spill registers
				BL read_character
				LDMFD SP!, {r1-r12, lr}   ; Restore registers
				
				CMP r0, #0x51		; Check if entered ASCII character is 'Q'
				BEQ q_label			
				
				CMP r0, #0x30		; Check if entered ASCII character is '0'
				BEQ display_0		
				
				CMP r0, #0x2B		; Check if entered ASCII character is '+'
				BEQ increment_7_seg
				
				CMP r0, #0x2D		; Check if entered ASCII character is '-'
				BEQ decrement_7_seg
				
				B UART0_END
				
display_0		; Display '0' to PuTTY
				STMFD SP!, {r0-r12, lr}
				MOV r0, #0x30
				BL output_character
				LDMFD SP!, {r0-r12, lr}
				
display_0_to_7_seg		; Display '0' to 7-seg
				MOV r0, #0
				LDR r1, =SEGSTATE			
				STRB r0, [r1]				; Store 0 in SEGSTATE
				STMFD SP!, {r0-r12, lr}   ; Spill registers
				BL display_digit_on_7_seg
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				B UART0_END
				
				; Display '9' on 7-seg
display_9		MOV r0, #9
				LDR r1, =SEGSTATE
				STRB r0, [r1]
				STMFD SP!, {r0-r12, lr}   ; Spill registers
				BL display_digit_on_7_seg
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				B UART0_END

increment_7_seg 
				; display '+' to screen
				STMFD SP!, {r0-r12, lr}
				MOV r0, #0x2B
				BL output_character
				LDMFD SP!, {r0-r12, lr}
				
				; Check if current state is '9'
				; If yes display '0', else increment
				LDR r1, =SEGSTATE		; Load 7-seg current character 
				LDRB r2, [r1]
				MOV r3, #0x9			
				CMP r2, r3				; Check to see if current character is 9
				BEQ display_0_to_7_seg	; If current value was 9 wrap around to 0
				
				; Increment SEGSTATE and display next digit on 7 seg
				ADD r2, r2, #1			; Increment SEGSTATE
				STRB r2, [r1]
				MOV r0, r2
				STMFD SP!, {r0-r12, lr}   ; Spill registers
				BL display_digit_on_7_seg
				LDMFD SP!, {r0-r12, lr}   ; Restore registers
				B UART0_END
				
decrement_7_seg 
				; display '-' to screen
				STMFD SP!, {r0-r12, lr}
				MOV r0, #0x2D
				BL output_character
				LDMFD SP!, {r0-r12, lr}

				; Check if current value in SEGSTATE is 0
				LDR r1, =SEGSTATE		; Load 7-seg current character 
				LDRB r2, [r1]
				MOV r3, #0x0		
				CMP r2, r3				; Check to see if current character is 0
				BEQ display_9			; If current value was 0 wrap around to 9
				
				; Decrement SEGSTATE and display previous digit on 7 seg
				SUB r2, r2, #1			; Decrement SEGSTATE
				STRB r2, [r1]
				MOV r0, r2
				STMFD SP!, {r1-r12, lr}   ; Spill registers
				BL display_digit_on_7_seg
				LDMFD SP!, {r1-r12, lr}   ; Restore registers
				B UART0_END
				
only_Q			; Check if input from UART0 is 'Q' 
				STMFD SP!, {r1-r12, lr}   ; Spill registers
				BL read_character		; Get ASCII character input
				LDMFD SP!, {r1-r12, lr}   ; Restore registers
				CMP r0, #0x51			; Check if the input is "Q"
				BEQ q_label
				B UART0_END			

q_label			; display 'Q' to screen
				STMFD SP!, {r0-r12, lr}
				MOV r0, #0x51
				BL output_character
				LDMFD SP!, {r0-r12, lr}
				
				; Set value in Q_PRESSED to 1, indicating Q has been pressed
				LDR r0, =Q_PRESSED
				MOV r1, #1
				STRB r1, [r0]
				B UART0_END

				
UART0_END		LDMFD SP!, {r0-r12, lr}   ; Restore registers
				
FIQ_Exit		LDMFD SP!, {r0-r12, lr}
				SUBS pc, lr, #4

	END